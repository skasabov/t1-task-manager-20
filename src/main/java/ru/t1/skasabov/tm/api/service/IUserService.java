package ru.t1.skasabov.tm.api.service;

import ru.t1.skasabov.tm.enumerated.Role;
import ru.t1.skasabov.tm.model.User;

public interface IUserService extends IService<User> {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findByLogin(String login);

    User findByEmail(String email);

    User removeByLogin(String login);

    User removeByEmail(String email);

    void setPassword(String id, String password);

    void updateUser(String id, String firstName, String lastName, String middleName);

    Boolean isLoginExist(String login);

    Boolean isEmailExist(String email);

}
