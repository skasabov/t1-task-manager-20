package ru.t1.skasabov.tm.api.service;

import ru.t1.skasabov.tm.enumerated.Sort;
import ru.t1.skasabov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IService<M> {

    void clear(String userId);

    Boolean existsById(String userId, String id);

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator comparator);

    List<M> findAll(String userId, Sort sort);

    M findOneById(String userId, String id);

    M findOneByIndex(String userId, Integer index);

    Integer getSize(String userId);

    void removeById(String userId, String id);

    void removeByIndex(String userId, Integer index);

    M add(String userId, M model);

    M remove(String userId, M model);

}
