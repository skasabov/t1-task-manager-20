package ru.t1.skasabov.tm.api.repository;

import ru.t1.skasabov.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    Project create(String userId, String name, String description);

    Project create(String userId, String name);

}
