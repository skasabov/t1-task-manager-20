package ru.t1.skasabov.tm.api.model;

public interface ICommand {

    String getArgument();

    String getDescription();

    String getName();

    void execute();

}
