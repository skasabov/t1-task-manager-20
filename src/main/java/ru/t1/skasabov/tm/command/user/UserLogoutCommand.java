package ru.t1.skasabov.tm.command.user;

import ru.t1.skasabov.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    private static final String NAME = "logout";

    private static final String DESCRIPTION = "Logout current user.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        serviceLocator.getAuthService().logout();
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
