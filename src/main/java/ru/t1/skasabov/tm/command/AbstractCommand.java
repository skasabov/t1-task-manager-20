package ru.t1.skasabov.tm.command;

import ru.t1.skasabov.tm.api.model.ICommand;
import ru.t1.skasabov.tm.api.service.IAuthService;
import ru.t1.skasabov.tm.api.service.IServiceLocator;
import ru.t1.skasabov.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    public String getUserId() {
        return getAuthService().getUserId();
    }

    public abstract Role[] getRoles();

    @Override
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

}
