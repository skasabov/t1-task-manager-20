package ru.t1.skasabov.tm.command.user;

import ru.t1.skasabov.tm.api.service.IAuthService;
import ru.t1.skasabov.tm.enumerated.Role;
import ru.t1.skasabov.tm.model.User;
import ru.t1.skasabov.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    private static final String NAME = "user-registry";

    private static final String DESCRIPTION = "Registry user.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        final String email = TerminalUtil.nextLine();
        final IAuthService authService = serviceLocator.getAuthService();
        final User user = authService.registry(login, password, email);
        showUser(user);
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
