package ru.t1.skasabov.tm.exception.system;

public final class CommandEmptyException extends AbstractSystemException {

    public CommandEmptyException() {
        super("Error! Command is empty...");
    }

}
