package ru.t1.skasabov.tm.util;

import ru.t1.skasabov.tm.exception.field.NumberIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() {
        final String value = nextLine();
        try {
            return Integer.parseInt(value);
        }
        catch (final Exception e) {
            throw new NumberIncorrectException(value, e);
        }
    }

}
