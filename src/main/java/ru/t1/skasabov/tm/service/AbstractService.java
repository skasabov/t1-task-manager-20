package ru.t1.skasabov.tm.service;

import ru.t1.skasabov.tm.api.repository.IRepository;
import ru.t1.skasabov.tm.api.service.IService;
import ru.t1.skasabov.tm.enumerated.Sort;
import ru.t1.skasabov.tm.exception.entity.ModelEmptyException;
import ru.t1.skasabov.tm.exception.field.IdEmptyException;
import ru.t1.skasabov.tm.exception.field.IndexIncorrectException;
import ru.t1.skasabov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected final R repository;

    public AbstractService(final R repository) {
        this.repository = repository;
    }

    @Override
    public M add(final M model) {
        if (model == null) throw new ModelEmptyException();
        return repository.add(model);
    }

    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    public List<M> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return repository.findAll(sort);
    }

    @Override
    public M findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Override
    public M findOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= repository.getSize()) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);
    }

    @Override
    public M remove(final M model) {
        if (model == null) throw new ModelEmptyException();
        return repository.remove(model);
    }

    @Override
    public void removeById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.removeById(id);
    }

    @Override
    public void removeByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        repository.removeByIndex(index);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    @Override
    public Boolean existsById(final String id) {
        return repository.existsById(id);
    }

}
